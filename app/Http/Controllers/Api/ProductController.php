<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;

class productController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $products=Product::all()->where('is_active',1);
        return response()->json([
        'products'=>$products // Bring all the products in DB they are active
        ],200);
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        $request->validate([
            'name'        => 'unique|max:500',
            'description' => 'max:500',
            'price'       => 'integer|max:11',
            'slug'        => 'max:255',
            'is_active'   => 'boolean',

        ]);
// To verify that data coming from HTTP request meets the rules I want , so t put rules at the data came to products table
// I created an array called Data. I will save the values that come from the request , and then through the Create function, I will save this object in Database
        $data['name']        = $request->name;
        $data['description'] = $request->description;
        $data['price']       =  $request->price;
        $data['slug']        = $request->slug;
        $data['is_active']   = $request->is_active;
        if ($request->hasFile('image')) {
            $d_path='images/products';     // The image path where I will save it
            $image=$request->file('image');// Get image from Request
            $image_name=$image->getClientOriginalName();// Get the name of the original downloaded file
            $path=$request->file('image')->storeAs($d_path,$image_name);// To not make filename to be automatically assigned to your stored file
            $request->image->move($d_path,$image_name);//to copy an existing file to a new location on the project,
            $data['image'] =$image_name;// Save the name of the file coming from the request in the data array
            }
            //store the image in our project and in DB here the repeat the same image not save many time so in the, only one the same name, code below save the save image Infinite number
            // by the make the name of file = time() and aad the extension

// if ($request->hasFile('image')) {                          if we use this code to save the file will work perfectly
//  $file_ext=$request->image->getClientOriginalExtension() ; if we use this code to save the file will work perfectly
// $file_name=time().'.'.$file_ext;                           if we use this code to save the file will work perfectly
//  $path='images/product';                                   if we use this code to save the file will work perfectly
//  $request->image->move($path,$file_name);                  if we use this code to save the file will work perfectly
// $data['image']=$file_name;                                 if we use this code to save the file will work perfectly
//         }
        $product= Product::create($data);//Create an object in the Product table using the Product model
        $product=Product::where('name', $request->name)->first();// The where method will check if the value of the name coming from the same request in the database will display it. It is normal for each product to have a different name.
        return response()->json([ //Return the saved object with a json format
            'status' => 201,
            'message' => 'product Created Successfully',
            'product' => $product,
        ]);

    }



    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $product=Product::find($id); // bring object from DB by his id
        if($product){ // If the product is in the database, enter the condition
        return  response()->json([
            'product'=>$product // Return the saved object with a json format
            ],200);
        }

       return  response()->json([], 204); //

    }
    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $product=Product::findOrFail($id);
        if( $request->name !=null){
            $product->name = $request->name; // I put condition if request that came from user null does not change any value and if the value from request not null save the new value it in DB
        }
        if($request->description !=null){
            $product->description = $request->description;
        }
        if( $request->price  !=null){
            ($product->price = $request->price);
        }
        if( $request->slug  !=null){
            $product->slug   = $request->slug;
        }
        if($request->is_active  !=null){
            $product->is_active = $request->is_active;
         }

        if ($request->hasFile('image')) {
            $d_path='images/products';
            $old_image=$request->file('image');
            $image_name=$old_image->getClientOriginalName();//
            $path=$request->file('image')->storeAs($d_path,$image_name);//
            $request->avatar->move($d_path,$old_image);//
            $data['image'] =$image_name;//


 }


//  $product->update($data);

$product->save();//save() method is used both for saving new model, and updating existing one
 return response()->json([//
     'status'=>200,
     'data'=>$product,
     'message' => 'product Updated Successfully',
 ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $product =Product::findOrFail($id);// bring object from DB by id
        $product->delete(); // delete the prev object
        return response()->json([
            'status'=>410,
            'message' => 'Request Information deleted Successfully',
        ],410);// Returns a message that the deletion was completed
    }
    public function showPrice($type='normal')
    {
// Here I have given up more than one scenario and method for the solution, and it is possible through relationships according to the task. We have a product that connects with users according to their type, asking for a specific price. Here I assumed that the manager has a fixed price, and in the case of the type, the gold user is less than half, the silver user is less than a quarter, and the normal is the same as the basic price.
// I used the function showPrice  and passed a parameter from the request that specifies the type of user
//    if($type =='gold'){ //User type is gold
//       $user=User::where('type',$type)->first(); Return the gold user data
//       $product=Product::where('id',$user->product_id)->first();I relied on the Product_id column that belongs to the golden user and compared it to the id that belongs to the product
//       return response()->json(
//         [ I returned the price of the product after the discount and its name
//             "product"=>$product->name,
//             "price"=>$product->price /2,
//         ]
//       );

// }
//     if($type =='silver'){ The same code as before, but here the price differs
//     $user=User::where('type',$type)->first();
//     $product=Product::where('id',$user->product_id)->first();

//     return response()->json(
//       [ Price for silver user
//           "product"=>$product->name,
//           "price"=>($product->price)* 3/4,
//       ]
//     );

// }
//     else{ Price for normal user
//         $user=User::where('type',$type)->first();
//         $product=Product::where('id',$user->product_id)->first();

//         return response()->json(
//             [
//                 "product"=>$product->name,
//                 "price"=>$product->price ,
//             ]
//           );
// }
// The second method: In the case of several products, we will use a loop to go through each object and deal with it individually
// $product=Product::find(2);
//  $users=$product->users;
// $len=count($product->users); numbers of object in DB used counter in the loop
// foreach($users as $user){ To deal with each user individually as an object
//     if($user->type =='gold'){
//  return response()->json([
// "product"=>$product->name,
// "price"=>$product->price/2
// ]);

// }
// if($user->type =='silver'){
//     return response()->json([
//    "product"=>$product->name,
//    "price"=>($product->price)*3/4
//    ]);

//    }

//    if($user->type =='normal'){
//     return response()->json([
//    "product"=>$product->name,
//    "price"=>$product->price/2
//    ]);

//    }
//     }
// I used the method (product) in User model inside method (with) to obtain product and user together
$user=User::with('product')->where('type',$type)->first();
if($user->type =='gold'){//
return response()->json([
    'product'=>$user->product->name,
    'price'=>$user->product->price /2,
]);
}
if($user->type =='silver'){
    return response()->json([
        'product'=>$user->product->name,
        'price'=>($user->product->price)*3/4,
    ]);
    }

    if($user->type =='normal'){
        return response()->json([
            'product'=>$user->product->name,
            'price'=>$user->product->price ,
        ]);
        }
    }

    }

