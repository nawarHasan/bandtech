<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{

    public function register(Request $request){

        $request->validate([
            'username' => 'required|unique|max:255',
            'name'     => 'required',
            'password' => 'required|min:6|max:50',
            'boolean'  => 'boolean'

        ]);
        $data['name']     = $request->name;
        $data['username'] = $request->username;
        $data['password'] =  Hash::make($request->input('password'));
        $data['type']     = $request->type;
        $data['is_active'] = $request->is_active;
        $data['username'] = $request->username;
        if ($request->hasFile('avatar')) {
            $d_path='images/users'; // publuc/images/users the path in project
            $image=$request->file('avatar');//
            $image_name=$image->getClientOriginalName();
            $path=$request->file('avatar')->storeAs($d_path,$image_name);
            $request->avatar->move($d_path,$image_name);
            $data['avatar'] =$image_name;
            }

        $device_name = $request->post('device_name','acces');
        $user= User::create($data);
        $token = $user->createToken($device_name);
        $user=User::where('username', $request->username)->first();
        return response()->json([
            'status' => 201,
            'message' => 'user Created Successfully',
            'token' => $token->plainTextToken,
            'user' => $user,
        ]);
    }
}
