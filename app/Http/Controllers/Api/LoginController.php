<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function login(Request $request) // use Request class to obtain a object used to interact with http request came to this request
{
     $request->validate([
         'username' => 'required|max:255',
         'name'     => 'required',
         'password' => 'required|min:6|max:50',
         'boolean'  => 'boolean'

     ]);
// To verify that data coming from HTTP request meets the rules I want , so t put rules at the data came to user table when i login
    $user = User::where('username', $request->username)->first();// create object that match between username in DB and the value come from client-side
    if ($user && Hash::check($request->password, $user->password)) {// if $user not null and password came from user same (i use Hash class to hash password came from user because in DB password is hashed)
        $device_name = $request->post('device_name', 'acces');// here i start create token to use it in future with sanctum and add middleware for the project
        $token = $user->createToken($device_name);//createToken method for create the token and i can control by the token value from this method
        return \Illuminate\Support\Facades\Response::json([ // i display the data as json so i used this method
            'token' => $token->plainTextToken,
            'user'  => $user,
        ], 201); // give the server status 201 mean create
    }
    return \Illuminate\Support\Facades\Response::json([
        'code' => 0,
        'message' => 'invaled credentioal'
    ], 401);// if the data not valid


}
}
