<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $users=User::all();// //bring all the users in DB
        return response()->json([
        'users'=>$users //return the boject in json form
        ],200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
// To verify that data coming from HTTP request meets the rules I want , so t put rules at the data came to users table
// I created an array called Data. I will save the values that come from the request , and then through the Create function, I will save this object in Database
// same the register method in RegisterController
        $data['name']     = $request->name;
        $data['username'] = $request->username;
        $data['password'] =  Hash::make($request->input('password'));
        $data['type']     = $request->type;
        $data['is_active'] = $request->is_active;
        $data['username'] = $request->username;
        if ($request->hasFile('avatar')) {
            $d_path='images/users';
            $image=$request->file('avatar');
            $image_name=$image->getClientOriginalName();
            $path=$request->file('avatar')->storeAs($d_path,$image_name);
            $request->avatar->move($d_path,$image_name);
            $data['avatar'] =$image_name;
            }

        $device_name = $request->post('device_name','acces');
        $user= User::create($data);
        $token = $user->createToken($device_name);
        $user=User::where('username', $request->username)->first();
        return response()->json([
            'status' => 201,
            'message' => 'user Created Successfully',
            'token' => $token->plainTextToken,
            'user' => $user,
        ]);
    }


    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $user=User::find($id); // bring object from DB by his id
        if($user){             // if the user exists in the database. Enter the condition
        return  \Illuminate\Support\Facades\Response::json([
            'user'=>$user
            ],200);
        }

        return  response()->json([], 204);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $user=User::findOrFail($id); // bring object from DB by his id
   //I put condition if request that came from user null does not change any value and if the value from request not null save the new value it in DB
        if( $request->name !=null){
            $user->name = $request->name;
        }
        if($request->username !=null){
            $user->username = $request->username;
        }
        if( $request->password  !=null){
            $user->password = Hash::make($request->input('password'));
        }
        if( $request->type  !=null){
            $user->type   = $request->type;
        }
        if($request->is_active  !=null){
            $user->is_active = $request->is_active;
         }
        if( $request->is_active  !=null){
            $user->username = $request->username;
         }

        if ($request->hasFile('avatar')) {
            $d_path='images/users';
            $old_image=$request->file('avatar');
            $image_name=$old_image->getClientOriginalName();
            $path=$request->file('avatar')->storeAs($d_path,$image_name);
            $request->avatar->move($d_path,$old_image);
            $data['avatar'] =$image_name;
 }
$user->save();
 return response()->json([
     'status'=>200,
     'data'=>$user,
     'message' => 'User Updated Successfully',
 ]);
    }
    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $user =User::findOrFail($id);// bring object from DB by id
        $user->delete(); // delete the prev object
        return response()->json([
            'status'=>410,
            'message' => 'Request Information deleted Successfully',
        ],410); //  Returns a message that the deletion was completed
    }
}
