<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class Product extends Model
{
    use HasApiTokens, HasFactory, Notifiable;

// make the columns available to display
protected $fillable = ['name','description', 'image', 'slug','price','is_active',];

// make the columns hidden (not display) we use protected $hidden
//// OneToMany Relation ///////

public function users(){
    return $this->hasMany('App\Models\User','product_id','id');
    // return $this->hasMany('App\Models\User','product_id','x') x: if not id column

}
}
