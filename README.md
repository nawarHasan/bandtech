-Install:(Setting up the development environment on local machine) :
git clone https://gitlab.com/nawarHasan/bandtech.git,
composer install or update,
php artisan key:generate,
php artisan cache:clear && php artisan config:clear

-Usage:
in .env file :
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=bandTech
DB_USERNAME=root
DB_PASSWORD=

-In MYSQL:
create database bandTech

-Then Migrate the tables in the project terminal:
php artisan migrate


I make the collection and put it in the project the name(BandTech.postman_collection.json)
