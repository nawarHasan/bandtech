<?php

use App\Http\Controllers\Api\ProductController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
// Route::put('user/{id}',[\App\Http\Controllers\Api\UserController::class,'update']);

//// Start User Routes/////

// use Route class to conect the url betweem user and the project ,post:request method,name of method inside Controller will be call
Route::post('/login', [App\Http\Controllers\Api\LoginController::class,'login']);
Route::post('/register', [App\Http\Controllers\Api\RegisterController::class,'register']);
// i use apiResource method give all essential routes this function make the code easiers
Route::apiResource('user',\App\Http\Controllers\Api\UserController::class);
////End User Routes//////

////Start Product Routes///////
Route::apiResource('product',\App\Http\Controllers\Api\ProductController::class);
Route::get('price/{type?}',[productController::class,'showPrice'])->where('type','[a-z]+');
// gives regex pattern for route parameter should match with it
//type parameter will save in the showPrice($type)
/////End Product Routes///////


